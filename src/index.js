import React from 'react'
import 'antd/dist/antd.css';

import styled, { css } from 'styled-components'

import { Avatar } from 'antd'

const Paragraph = styled.p`
  color: chocolate;
  padding-top: 6px;
  margin-bottom: 0px;
`

const Button = styled.button`
  background: transparent;
  border-radius: 6px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0.5em 1em;
  padding: 0.25em 1em;
  cursor: pointer;

  ${props => props.primary && css`
    background: palevioletred;
    color: white;
  `}
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: beige;
  border-radius: 6px;
  padding-top: 10px;
  padding-bottom: 10px;
`


const App = ({ name, phone, src, onDelete, onCallUser }) => {

  const handleOnClickCallUser = () => {
    
    onCallUser(phone)
    
  }

  const handleOnClickDelete = () => {
    
    onDelete({name, phone})
    
  }

  

  return (    
    <div  style={{ width: 200, marginTop: 20, marginLeft: 20 }}>
      <Container>
        <Avatar size={80} src={src} />
        <Paragraph>{name}</Paragraph>
        <Button onClick={handleOnClickCallUser} >Llamar a usuario</Button>
        <Button onClick={handleOnClickDelete} primary>Eliminar usuario</Button>
      </Container>
    </div>       
  )

}

export default App
