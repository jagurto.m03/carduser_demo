import React from 'react'
import {render} from 'react-dom'


import Example from '../../src'

const App = () => {

  const  handleOnCallUser = phone => {

    console.log(phone)
  }

  const handleOnDelete = props => {
    let { name, phone } = props;

    console.log(`${name} ${phone}`)
  }

  const data = [
    {
      name: 'Gene',
      age: '25',
      src: 'https://appsdejoseluis.com/wp-content/uploads/2020/04/face_co.png',
      phone: '+34655999999'
    },
    {
      name: 'Carolina',
      age: '28',
      src: 'https://previews.123rf.com/images/yupiramos/yupiramos1709/yupiramos170910686/85481627-avatar-mujer-retrato-de-la-mujer-ilustraci%C3%B3n-imagen-del-vector.jpg',
      phone: '+584145556666'
    },
    {
      name: 'Carlos',
      age: '42',
      src: 'https://appsdejoseluis.com/wp-content/uploads/2020/04/face_co.png',
      phone: '+34655888888'
    },
  ];

  return (
    <div>

      {
        data.map((v, i) => (

          <Example 
          key={i}
          name={v.name}
          phone={v.phone} 
          src={v.src}
          onDelete={handleOnDelete}
          onCallUser={handleOnCallUser}/>

        ))
      }
      
      
    </div>
  )

}

render(<App/>, document.querySelector('#demo'))
